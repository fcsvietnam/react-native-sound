package com.zmxv.RNSound;

import android.media.MediaDataSource;
import android.os.Build;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.RequiresApi;

import java.io.IOException;

@RequiresApi(api = Build.VERSION_CODES.M)
public class Base64AudioDataSource extends MediaDataSource {
    private byte[] decodedBytes ;
    public Base64AudioDataSource(String base64){
        decodedBytes = Base64.decode(base64,Base64.NO_WRAP);
        Log.d("Base64","Base64 Data:"+base64);
        Log.d("Base64","Bytes Length"+decodedBytes.length);

    }

    @Override
    public synchronized long getSize() throws IOException {
        synchronized(decodedBytes){
            return decodedBytes.length;
        }
    }

    @Override
    public void close() throws IOException {

    }

    @Override
    public synchronized int readAt(long position, byte[] bytes, int offset, int size) throws IOException {
        synchronized(decodedBytes){
            int length = decodedBytes.length;
            if (position >= length) {
                return -1; // -1 indicates EOF
            }
            if (position + size > length) {
                size -= (position + size) - length;
            }
            Log.d("Base64","Position :"+position);
            System.arraycopy(decodedBytes, (int)position, bytes, offset, size);
            return size;
        }
    }
}
